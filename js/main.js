import * as Preloader from './preloader.js';
import * as Screens from './screens.js';

let app;

window.onload = function() {
    loadApplication();
}

function loadApplication() {
    app = new PIXI.Application({
        width: 800,
        height: 600,
        backgroundColor: 0x000000
    });
    document.body.appendChild(app.view);
    Screens.createCenterScreen(app);
    Preloader.loadAssets(app);
    document.addEventListener("loadingDone", () => {
       Screens.hideCenterScreen();
       document.removeEventListener("loadingDone", () => {});
    });
}

