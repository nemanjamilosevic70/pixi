let centerScreenText;
let centerScreen;

export function createCenterScreen(app) {
    centerScreen = new PIXI.Container();
    centerScreenText = new PIXI.Text("Loading...");
    centerScreenText.x = app.view.width / 2;
    centerScreenText.y = app.view.height / 2;
    centerScreenText.anchor.set(0.5);
    centerScreenText.style = new PIXI.TextStyle({
        fill: 0x00FF00,
        fontSize: 40,
        fontFamily: 'Arcade'
    });
    centerScreen.addChild(centerScreenText);
    app.stage.addChild(centerScreen);
}

export function setCenterScreenText(text) {
    centerScreenText.text = text;
}

export function showCenterScreen() {
    centerScreen.visible = true;
}

export function hideCenterScreen() {
    centerScreen.visible = false;
}
