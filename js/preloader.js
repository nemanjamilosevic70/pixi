

export function loadAssets(app) {
    app.loader.baseUrl = 'assets';
    app.loader
    .add('symbol1', 'eggHead.png')
    .add('symbol2', 'flowerTop.png')
    .add('symbol3', 'helmlok.png')
    .add('symbol4', 'skully.png')
    .load();
    app.loader.onProgress.add(showProgress);
    app.loader.onComplete.add(doneLoading);
    app.loader.onError.add(reportError);
}

function showProgress(e) {
    // console.log(e.progress);
}

function reportError(e) {
    console.log('ERROR: ' + e.message);
}

function doneLoading(e) {
    const loadingEvent = new Event("loadingDone");
    document.dispatchEvent(loadingEvent);
    console.log('Loading done!');
}